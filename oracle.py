from Crypto.Cipher import DES
import random

# some crazy DES key
key = "Cr4ZyK3y"
# some random initialisation vector
iv = "".join([str(random.randrange(9)) for i in range(8)])


'''
    Pad plaintext according to PKCS#7 standard
'''
def PKCS7( plaintext ):
    # Calculate padding value
    padbytes = 8 - len(plaintext) % 8
    # Create padding
    pad = padbytes * chr(padbytes)
    # Append padding
    return plaintext + pad 

'''
    This is the oracle, the decryptyption routine returns
    the result of this function. Since this function leaks
    information about padding validity, we can break *any*
    CBC encryptyption standard.
'''
def isValidPKCS7( plaintext ):
    length = len(plaintext)
    pval = ord(plaintext[length-1])  
    # If the padding value is incorrect                     
    if (pval > 8) or (pval < 1):
        return False
    # If there's the wrong number of pads
    if plaintext[length-pval:] != chr(pval)*pval:
        return False
    # Otherwise return plain
    return plaintext

'''
    Decrypt ciphertext using DES-CBC
'''
def decrypt( ciphertext ):
    cipher = DES.new(key, DES.MODE_CBC, iv)
    return isValidPKCS7(cipher.decrypt(ciphertext))

'''
    Encrypt plaintext using DES-CBC
'''
def encrypt( plaintext ):
    cipher = DES.new(key, DES.MODE_CBC, iv)
    ciphertext = cipher.encrypt(PKCS7(plaintext))
    return ciphertext


'''
    Perform an oracle attack using two consecutive blocks 
    of ciphertext. 
'''
def oracleAttack( prevblock, block ):
    # Create ciphertext in the form 
    # \x00\x00\x00\x00\x00\x00\x00\x00 + BLOCK
    # AKA  cprime + block
    cprime = chr(0)*8 
    # Store intermediate values as we solve them
    ivals = []
    # Store cracked plain text chars as we solve them
    plain = []
    # For each byte in cprime, starting at the last
    for cprime_idx in range(7,-1,-1):
        # Create a PKCS#7 padding index [0x01, 0x08]
        padding_idx = 8 - cprime_idx
        # Try each byte value until we get a valid padding
        # according to the oracle
        for guess in range(256):
            # Create new ciphertext with the guess
            if cprime_idx > 0:
                ciphertext = cprime[:cprime_idx]
                ciphertext += chr(guess)
            else:
                ciphertext = chr(guess)
            # Insert the previous intermediate values
            for intr in ivals:
                # Adjust them for this padding index
                ciphertext += chr(intr^padding_idx) 
            # Append the block we're cracking
            ciphertext += block
            # If the oracle correctly decrypts the ciphertext
            if decrypt( ciphertext ):
                # Calculate the intermediate value
                intermediate = guess^padding_idx
                # Save it
                ivals.insert(0, intermediate )
                # Crack the plain text character
                plain.insert(0, intermediate^ord(prevblock[cprime_idx]))
                # We found it, bail out
                break

    print "\n"+"Intermediate values =", ivals
    print
    plainstr = ""
    for c in plain:
        if c <= 0x08: plainstr += chr(c).encode("hex")
        else: plainstr += chr(c)
    return plainstr





if __name__ == '__main__':
    # Create a message that will reulst in two blocks
    # In other words, make it 8 > length < 16
    ct = encrypt("1234567 Hello!")
    # Split the ciphertext into two blocks of 8
    blk1 = ct[:8]
    blk2 = ct[8:]
    # Print some stuff out proving it
    print
    print "Encrypting text using DES-CBC...\n",
    print "\"1234567 Hello!\"::"+blk1.encode("hex")+" "+blk2.encode("hex")+"\n"
    print "Cracking ciphertext block ["+blk2.encode("hex")+"]"
    # Crack the second block using the first
    plain = oracleAttack( blk1, blk2 )
    print "Cracked ", blk2.encode("hex"), " to",
    print "\""+plain+"\""