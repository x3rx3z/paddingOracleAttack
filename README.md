
Depends on pycrypto - `pip install pycrypto`

```sh
Encrypting text using DES-CBC...
"1234567 Hello!"::d9129f6a89f0e006 84023c52d6505b42

Cracking ciphertext block [84023c52d6505b42]

Intermediate values = [145, 119, 243, 6, 230, 209, 226, 4]

Cracked  84023c52d6505b42  to  " H e l l o ! 02 02 "
```